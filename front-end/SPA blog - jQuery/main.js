// Shorthand for $( document ).ready()
$(function(){
    $('#load-btn').hide();
    start();
});


function start() {
    
    // array to hold all posts 
    var posts = [];
    
    // number of posts visible on page,  will be incremneted on each post addition
    var visiblePostCounter = 0;
    
    // number of post per load/page
    var postsPerPage = 10;
    
    var isFirsttAccess = true;
    
    // get posts data
    getPosts();
    
    // gets post json,  store them in array, then populate posts (postPerPage)
    function getPosts(){
        var url = 'https://jsonplaceholder.typicode.com/posts';
        function postsJsonCallback(json) {
            posts = json;
            // Manually trigger a hashchange to start the app.
            $(window).trigger('hashchange');
        }
        getJsonData(url, postsJsonCallback);
    }

    
//////////////////////////////////////////////////////////////   
    // An event handler with calls the render function on every hashchange.
    // The render function will show the appropriate content of out page.
    $(window).on('hashchange', function(){
        render(decodeURI(window.location.hash));
    });
    
    function render(url) {
        // Get the keyword from the url.
        var temp = url.split('/')[0];

        // routes
        var	map = {
            // The "Homepage".
            '': function() {
                if(isFirsttAccess) {
                    populatePosts(postsPerPage);
                    isFirsttAccess = false;
                }
            },

            // Single Post page.
            '#posts': function() {
                // Get the index of which post we want to show and call the appropriate function.
                var id = url.split('#posts/')[1].trim();
                displaySinglePost(id);
            }
        };

        // Execute the needed function depending on the url keyword (stored in temp).
        if(map[temp]){
            map[temp]();
        }
        // If the keyword isn't listed in the above - log error
        else {
            console.log('Error loading Page')
        }
    }
    
    //
    function createQueryHash(){
        window.location.hash = '#';
    }
    
    // ajax to get JSON
    function getJsonData(url, successCallback) {
        $.getJSON(url)
            .done(successCallback)
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            });
    }  
//////////////////////////////////////////////////////////////

    
/////////////////////////////////////////////
    // render homepage
    function populatePosts(perPage) {
        var postsPanel= $('#posts-panel');
        for(var i = 0; i < perPage; i++ ) {

            if(visiblePostCounter>=posts.length){
                $('#load-btn').hide();
                return;
            }

            var id = posts[visiblePostCounter].id;
            var title = posts[visiblePostCounter].title;
            var body = posts[visiblePostCounter].body;
            var commentTemplete = $([
                '<div class="card col-12 post" id="p'+id+'">',
                '  <h4 class="card-header bg-dark text-white">'+title+'</h4>',
                '  <div class="card-body">',
                '    <p class="card-text">'+body+'</p>',
                '  </div>',
                '</div>'
            ].join('\n'));
            postsPanel.append(commentTemplete);

            visiblePostCounter++;
        }
        $('#load-btn').show();
    }
/////////////////////////////////////////////
    
    
/////////////////////////////////////////////
    //render single post
    function displaySinglePost(postid){
        $('#details-modal').modal('show');
        getSinglePost(postid);
    }


    function getSinglePost(id) {
        var url = 'https://jsonplaceholder.typicode.com/posts/'+id;
        function singlePostJsonCallback(json) {
            getCommentsfor(id);
            populateSinglePostHead(json);
        }
        getJsonData(url, singlePostJsonCallback);
    }

    function populateSinglePostHead(post) {
        var id = post.id;
        var title =post.title;
        var body = post.body;

        var postTemplate = $([
            '<div class="card  bg-dark text-white" id="dp-'+id+'">',
            '  <div class="card-body">',
            '    <h4 class="card-title">'+title+'</h4>',
            '    <hr>',
            '    <p class="card-text">'+body+'</p>',
            '  </div>',
            '</div>'
        ].join('\n'));

        $('#post-text').append(postTemplate);
    }

    function getCommentsfor(id) {
        var url = 'https://jsonplaceholder.typicode.com/comments?postId='+id;
        function commentsJsonCallback(json) {
            populateCommnets(json);
        }
        getJsonData(url, commentsJsonCallback);
    }

    function populateCommnets(comments){
        var commentsPanel = $('#comments-panel');

        for(var c = 0; c < comments.length; c++) {
            var id = comments[c].id;
            var email = comments[c].email;
            var body = comments[c].body;

            var commnetTemplate = $([
                '<div class="card mb-3 comment" id="c'+id+'">',
                '  <div class="card-body">',
                '    <p class="card-text">'+body+'</p>',
                '  </div>',
                '<div class="card-footer">',
                '  <small class="text-muted">Posted by '+email+'</small>',
                '  </div>',
                '</div>'
            ].join('\n'));

            commentsPanel.append(commnetTemplate);
        }
    }
/////////////////////////////////////////////
    
    $('#load-btn').on('click', function(){
        populatePosts(postsPerPage);
    });

    $('#posts-panel').on('click','.post', function() {
        var id = $(this).attr('id').slice(1);
        window.location.hash = 'posts/' + id;
    });
    
    $('#details-modal').on('hidden.bs.modal', function (e) {
        $('#post-text').html('');
        $('#comments-panel').html('');
        createQueryHash();
    });
    
} // end start()
