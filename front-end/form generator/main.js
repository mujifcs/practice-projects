//Created by: Mujahid Akbar
//in Training session


var elementSelector = document.querySelector('#element-selector');
var elementOptionsField = document.querySelector('#element-options-field');

var inputTypes = ['text', 'email', 'password', 'button', 'radio', 'checkbox'];


// array to hold form fields
var formFields = [];

elementSelector.onchange = function() {
    // get selected option value
    var element = elementSelector[elementSelector.selectedIndex].value;
    cleareElementOptionsFieldData();
    if(element) {
        displayElementOptions(element);
    }
};

function displayElementOptions(element) { 
    //ask for label of the element
    createAndAppendDivFor('Label', 'labelValue');
    
    //ask for attributes
    if(element === 'select' || element === 'radio' || element === 'checkbox') {
        askForMultipleOptions(element);
    }
    else {
        createAndAppendDivFor('placeholder', 'placeholderValue');
        if(element !== 'password' && element !== 'textarea' && element !== 'email') {
            createAndAppendDivFor('Initial Value', 'valueValue');
        }
    }
    
    // textarea extra attributes
    if(element === 'textarea') {
        createAndAppendDivFor('Rows', 'rowsValue');
        createAndAppendDivFor('Cols', 'colsValue');
    }
   
    // button for adding element to form 
    var div = document.createElement('div');
    elementOptionsField.appendChild(div);
    
    var addElementBtn = document.createElement('button');
    addElementBtn.textContent = 'Add to form';
    div.appendChild(addElementBtn);    
    
    addElementBtn.onclick = function() {
        addToForm(element);   
    };
}

// ask for options
function askForMultipleOptions(element) {
    var div = document.createElement('div');
    elementOptionsField.appendChild(div);

    var optionInput = document.createElement('input');
    optionInput.setAttribute('placeholder', element+' option text');
    div.appendChild(optionInput);
    
    var addOptionButton = document.createElement('button');
    addOptionButton.textContent = 'Create '+element+' option';
    div.appendChild(addOptionButton);
    
    // div to display created options
    var optionsDiv = document.createElement('div');
    elementOptionsField.appendChild(optionsDiv);
    
    // ul conating options
    var ul = document.createElement('ul');
    ul.setAttribute('id', 'optionsList');
    optionsDiv.appendChild(ul);
    
    addOptionButton.onclick = function() {
        var optionValue = optionInput.value.trim();
        optionInput.value = '';
        if(optionValue.length) {
            var listItem = document.createElement('li');
            var listText = document.createElement('span');
            var listBtn = document.createElement('button');

            listItem.appendChild(listText);
            listText.textContent = optionValue;
            listItem.appendChild(listBtn);
            listBtn.textContent = 'Remove';
            ul.appendChild(listItem);

            listBtn.onclick = function() {
              ul.removeChild(listItem);
            }
        }
        optionInput.focus();
    }
}


// add field object to form array
function addToForm(element) {
    var field = {};
    var tag;
    var attributes = {};
    
    // get label value
    var label = document.querySelector('#labelValue').value;
    
    // get tag name / type / textarea extra attr
    if (inputTypes.indexOf(element) !== -1) {
        tag = 'input';
        attributes['type'] = element;
    }
    else if(element === 'select') {
        tag = element;
    }
    else if(element === 'textarea') {
        tag = element;
        var rows = document.querySelector('#rowsValue').value;
        var cols = document.querySelector('#colsValue').value;
        if(rows) {
            attributes['rows'] = rows;
        }
        if(cols){
            attributes['cols'] = cols;
        } 
    }
    
    
    if(element === 'select' || element === 'radio' || element === 'checkbox'){
        var optionsList = document.querySelector('#optionsList').querySelectorAll('span');
        if(optionsList.length) {
            var options = [];
            for(var i=0; i<optionsList.length; i++) {
                options.push(optionsList[i].textContent);
            }
            // add options array to field object
            field['options'] = options;
        }
        else {
            // do nothing
            return;
        }       
    }
    else {
        // get value attributes
        if(element !== 'password' && element !== 'textarea' && element !== 'email') {
            var value = document.querySelector('#valueValue').value;
            if(value.trim().length){
                attributes['value'] = value.trim();
            }
        }
        
        // get placeholder attributes
        var placeholder = document.querySelector('#placeholderValue').value;
        if(placeholder.trim().length){
            attributes['placeholder'] = placeholder.trim();       
        }
    }
    
    field['label'] = label;
    field['tag'] = tag;
    field['attributes'] = attributes;
    
    // add to formFields data
    formFields.push(field);
    console.log(element+ ' added to form');
    
    // this will reset the fields data and display options for the same elemet
    elementSelector.onchange();
    
    // preview form
    previewForm();
    
}


function previewForm() {
    var formPreviewField = document.querySelector('#form-preview-field');
    formPreviewField.textContent = '';
    
    // build form
    var form = document.createElement('form');
    formPreviewField.appendChild(form);
    
    
    for(var i =0; i<formFields.length; i++) {
        // build div to hold label and field
        var div = document.createElement('div');
        form.appendChild(div);
        
        // get the field
        var field = formFields[i];
        
        //build label
        var label = document.createElement('label');
        label.textContent = field.label;
        label.setAttribute('for',i);
        div.appendChild(label);
        
        // if select , checkbox or radio then options field is set
        if(field.options){
            
            if(field.tag === 'select') {
                var select  = document.createElement(field.tag);
                select.setAttribute('id', i);
                select.setAttribute('name', field.tag+i);
                
                for(var op = 0; op < field.options.length; op++) {
                    var option = document.createElement('option');
                    option.setAttribute('value',field.options[op]);
                    option.textContent = field.options[op];
                    select.appendChild(option);
                }
                div.appendChild(select);
            }
            else if(field.attributes.type === 'checkbox' || field.attributes.type === 'radio' ) {
                for(var item = 0; item < field.options.length; item++) {
                    
                    // sapn to hold each option and its text
                    var span = document.createElement('span');
                    div.appendChild(span);
                    
                    var input = document.createElement(field.tag);
                    input.setAttribute('value', field.options[item]);
                    input.setAttribute('type', field.attributes.type);
                    input.setAttribute('name', field.attributes.type+i);
                    span.appendChild(input);
                    span.appendChild(document.createTextNode(field.options[item]));
                }
            }
        }
        else {
            // build tag
            var tag = document.createElement(field.tag);
            tag.setAttribute('id', i);
            div.appendChild(tag);

            // set attributes
            for(var key in field.attributes) {
                tag.setAttribute(key, field.attributes[key]);
            }  
        }
    }
}

function createAndAppendDivFor(labelText, inputID) {
    var div = document.createElement('div');
    elementOptionsField.appendChild(div);
    
    var label = document.createElement('label');
    label.textContent = labelText+': ';
    div.appendChild(label);
    
    var labelValueInput = document.createElement('input');
    labelValueInput.setAttribute('id', inputID); 
    div.appendChild(labelValueInput);
}

function cleareElementOptionsFieldData() {
    elementOptionsField.textContent = '';
//    console.log('cleareElementOptionsFieldData');
}