/*
    Created By: Mujahid Akbar
    Facebook like Comment editor
*/


/*
    ***** Comments Stucture *****
    <section id='comments-panel'>
    
         <div id='Commet-id#-here'>
            <p>
                Commet text here
            </p
            <button title="delete">&#215;</button>
            <button title="edit">&#9997;</button>
         <div>
                .
                .
                .
    </section>
*/


// change it on edit button comment
var isEditMode = false;
// set this id of the editable comments' id
var editID = -1;

// the comment counter, used to assign id to new comment
var counter = 0;






var commentsPanel = document.querySelector('#comments-panel');
var editorPanel = document.querySelector('#editor-panel');
var editor = document.querySelector('#editor');
var postBtn = document.querySelector('#postBtn');



// create new comment
function createComment() {
    var commentText = editor.value.trim();
    editor.value = '';
    
    if(!commentText.length) {
        console.log('can not create empty commnet!!!');
        return;
    }
    
    var id = counter;
    counter++;
    
    var div = document.createElement('div');
    div.setAttribute('id',id);
    commentsPanel.appendChild(div);
    
    var p = document.createElement('p');
    p.textContent = commentText;
    div.appendChild(p);
    
    var deleteBtn = document.createElement('button');
    deleteBtn.innerHTML = '&#215;';
    deleteBtn.setAttribute('title', 'delete');
    div.appendChild(deleteBtn);
    deleteBtn.onclick = deleteComment;
    
    var editBtn = document.createElement('button');
    editBtn.innerHTML = '&#9997;';
    editBtn.setAttribute('title','edit');
    div.appendChild(editBtn);
    editBtn.onclick = editComment;
    
    editor.focus();
    console.log('Created');
}

function deleteComment(e) {
    var id = getTargetCommentId(e); 
    var comment = document.getElementById(id);  
    commentsPanel.removeChild(comment);
    console.log('Deleted');
}

function getTargetCommentId(e){
    return  e.target.parentNode.getAttribute('id');
}

function editComment(e) {
    isEditMode = true;
    editID = getTargetCommentId(e);
    var div = document.getElementById(editID);
    var text = div.firstChild.textContent;
    
    editor.value = text;
    editor.focus();
    postBtn.textContent = 'Save';
    postBtn.setAttribute('title', 'save changes');
    
    var cancelBtn = document.createElement('button');
    cancelBtn.textContent = 'Cancel';
    cancelBtn.setAttribute('title', 'Cancel Changes');
    cancelBtn.setAttribute('id','cancelBtn');
    cancelBtn.onclick = function(){
        isEditMode = false;
        editID = -1;
        editorPanel.removeChild(cancelBtn);
        editor.value = '';
    }
    editorPanel.appendChild(cancelBtn);
}

function updateComment(){
    isEditMode = false;
    var div = document.getElementById(editID);
    editID = -1;
    
    var commentText = editor.value.trim();
    editor.value = '';
    
    if(commentText) {
        div.firstChild.textContent = commentText;
        div.focus();
        console.log('Updated');
    }
    else {
        commentsPanel.removeChild(div);
    }
    editorPanel.removeChild(document.getElementById('cancelBtn'));
    postBtn.textContent = 'Post';
    postBtn.setAttribute('title', 'post commnet');
}

function postComment() {
    if(isEditMode) {
        updateComment();
    }
    else {
        createComment();
    }
}

// attach event handler to enter key press in editor 
editor.addEventListener('keyup', function(e) {
    if(e.keyCode === 13){
        postComment();
    }
});

// attach same event handler as of editor to postBtn 
postBtn.onclick = postComment;












// objec to hold comments data
//var data = {
//    
//    createComment : function(e) {
//        
//    },
//    editComment : function(e) {
//        console.log('edit '+e);
//    },
//    deleteComment : function(e, id) {
//        console.log('delete '+e);
//    },
//    postComment : function() {
//        if(isEditMode) {
//            this.editComment();
//        }
//        else {
//            // creates problem with functions this 
//            this.createComment();
//        }
//    }
//}