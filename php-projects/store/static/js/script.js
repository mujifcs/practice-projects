
$(function() {
    init();
});


function init() {
    $('.btn-add-item').on('click', function(){
        var id = $(this).attr('id');
        var url = 'http://example.com/index.php/cart/add';
        var postdata = {id : id, qty : 1, single : true};

        post(url, postdata);
        
    });
    
    $('.btn-remove-item').on('click', function(){
        var id = $(this).attr('id');
        var postdata = {id : id};
        
        var itemTotal = $('#r-'+id+' .item-total').html();
        itemTotal = parseInt(itemTotal);
        
        var total = $('#total').html();
        total = parseInt(total);
        
        var newTotal = total - itemTotal;
        
        var url = 'http://example.com/index.php/cart/remove';
        var postdata = {id : id};
        
        var success = function(data) {
            data = jQuery.parseJSON(data);
            if(data['success']) {
                $('#total').html(newTotal);
                $('#r-'+id).remove();
            }
        };
        
        post(url, postdata, success);
        
    });
    
    
    $('tr .qty').on( "change", function(){
        var rowid = $(this).closest( "tr" ).attr('id');
        var id = rowid.split("-")[1];
        var qty = $(this).val();
        
        var postdata = {id : id, qty : qty};
        
        var url = 'http://example.com/index.php/cart/add';
        
        var success = function(data) {
            data = jQuery.parseJSON(data);
            if(data['success']) {
                var price = $('#'+rowid+' .price').html();
                price = parseInt(price);
                var itemTotal = qty*price;

                var oldItemTotal = $('#'+rowid+' .item-total').html();
                oldItemTotal = parseInt(oldItemTotal);
                
                $('#'+rowid+' .item-total').html(itemTotal);
                
                var total = $('#total').html();
                total = parseInt(total);
                
                var newTotal = total + itemTotal - oldItemTotal;
                
                $('#total').html(newTotal);
            }
        };
        
        var fail = function(data) {
            console.log('fail');
        };
        
        post(url, postdata, success, fail);
    });
    
    
    $('#btn-checkout').on('click', function(){
       $('#checkoutModal').modal('show'); 
    });
    
}



function post(url, postdata, success = function(){}, fail = function(){}, always = function(){}) {
    
    $.post(url, postdata)
        .done(success)
        .fail(fail)
        .always(always)
    ;
}