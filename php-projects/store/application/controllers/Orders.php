<?php
class Orders extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('orders_model');
    }

    public function index()
    {
        if(!isset($_SESSION['admin'])) {
            redirect('/', 'location', 302);
        }
        
        $data['orders'] = $this->orders_model->get_orders();

        $data['title'] = 'Orders';
        
        $this->load->view('templates/header', $data);
        $this->load->view('orders/index', $data);
        $this->load->view('templates/footer');
    }

    public function view($order_id = NULL)
    {
        $data['order_items'] = $this->orders_model->get_orders($order_id);
        
        if (empty($data['order_items']))
        {
                show_404();
        }

        $data['title'] = 'Order#'.$order_id;

        $this->load->view('templates/header', $data);
        $this->load->view('orders/view', $data);
        $this->load->view('templates/footer');
    }
    
    public function deliver($order_id = NULL) {
        
        if(!isset($_SESSION['admin'])) {
            redirect('/', 'location', 302);
        }
        
        $this->orders_model->update_status($order_id, 1);

        
        $_SESSION['success'] = 'Order is marked as delivered';
        
        redirect('orders/view/'.$order_id, 'location', 302);
    }
}
