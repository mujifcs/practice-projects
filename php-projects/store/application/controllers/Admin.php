<?php
class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index() {
        $this->login();
    }
    
    public function login() {
        
        if(isset($_SESSION['admin'])) {
            redirect('/', 'location', 302);
        }
        
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data['title'] = 'Admin Login';
        
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/index');
            $this->load->view('templates/footer');
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            if($email === 'admin@store.com' && $password === '12345') {
                $_SESSION['admin'] = 'admin';
                redirect('/', 'location', 302);
            }
            
            $_SESSION['loginError'] = 'Email or Password is incorrect';
            redirect('admin/login', 'location', 302);
            
        }
    }
    
    public function logout() {
        $_SESSION[] = array();
        
        session_destroy();
        redirect('/', 'location', 302);
    }
}
