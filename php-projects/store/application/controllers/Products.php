<?php
class Products extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('products_model');
    }

    public function index()
    {
        $data['products'] = $this->products_model->get_products();

        $data['title'] = 'Products';
        
        $this->load->view('templates/header', $data);
        $this->load->view('products/index', $data);
        $this->load->view('templates/footer');
    }

    public function view($product_id = NULL)
    {
        $data['products_item'] = $this->products_model->get_products($product_id);
        
        if (empty($data['products_item']))
        {
                show_404();
        }

        $data['title'] = $data['products_item']['product_name'];

        $this->load->view('templates/header', $data);
        $this->load->view('products/view', $data);
        $this->load->view('templates/footer');
    }
    
    public function create() {
        
        if(!isset($_SESSION['admin'])) {
            redirect('/', 'location', 302);
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->load->model('categories_model');
        
        $data['categories'] = $this->categories_model->get_categories();
        
        $data['title'] = 'New Product';
        
        $this->form_validation->set_rules('name', 'Product Name', 'required');
        $this->form_validation->set_rules('price', 'Product Price', 'required');
        $this->form_validation->set_rules('category', 'Category Name', 'required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('products/create');
            $this->load->view('templates/footer');
        }
        else
        {
            
            $product_img = $this->handleImage();
            
            $product_data = array(
                'product_name' => trim($this->input->post('name')),
                'product_price' => trim($this->input->post('price')),
                'category_id' => trim($this->input->post('category')),
                'product_img' => $product_img
            );
            
            $this->products_model->set_product($product_data);
            $_SESSION['createSuccess'] = 'Product created successfully!';
            
            redirect('products/create', 'location', 302);
        }
    }
    
    
    private function handleImage() {
        // file processing 
        $this->load->library('filehandler');
        if(!$this->filehandler->checkFile($_FILES['imgFile'])) {
            $_SESSION['createError'] = $this->filehandler->getMessages();
            redirect('products/create', 'location', 302);
        }

        // image processing
        $this->load->library('imageprocess');
        $filepath = $_FILES['imgFile']['tmp_name'];

        $imginfo = $this->imageprocess->getImageInfo($filepath);
        
        if($imginfo === false) {
            $_SESSION['createError'] = ['File is not an image!'];
            redirect('products/create', 'location', 302);
        }
        
        
        $ext = explode('/', $imginfo['mime'])[1];
        $name = time() . ".$ext";
        $img_dir = "static/uploads/";
        $thumbnail_dir = "static/uploads/thumbnails/";
        
        $result = $this->imageprocess->processImage($filepath, $imginfo['mime'], $name, $img_dir, $thumbnail_dir);
        
        if($result) {
            return $name;
        }
        
        $_SESSION['createError'] = ['Error while processing image, please try again!'];
        redirect('products/create', 'location', 302);
    }
}
