<?php
class Cart extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('products_model');
        
        if(!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }
    }

    public function index()
    {
        
        $data['cart_items'] =array();
        
        if(!empty($_SESSION['cart'])) {
            
            $product_ids = array_keys($_SESSION['cart']);
            $data['cart_items'] = $this->products_model->get_products(true, $product_ids);
        }
        
        $data['title'] = "Cart";
        $this->load->view('templates/header', $data);
        $this->load->view('cart/index', $data);
        $this->load->view('cart/checkout');
        $this->load->view('templates/footer');
    }

    
    public function add() {
        
        $product_id = (int)trim($this->input->post('id'));
        $qty = (int)trim($this->input->post('qty')); 
        $single = (boolean)trim($this->input->post('single'));
        
        // default quantity
        if($qty < 1) {
            $qty = 1;
        }
 
        // check if valid product _id
        $product = $this->products_model->get_products($product_id);
        
        // if no product found
        if(!$product){
            die('{"fail": true}');
        }
        
        
        if(empty(($_SESSION['cart'][$product_id]))) {
            
            $_SESSION['cart'][$product_id] = array(
                'product_id' => $product_id,
                'order_item_qty' => $qty    
            );
            
        } elseif($single) {
                $_SESSION['cart'][$product_id]['order_item_qty'] += $qty ;
        } else {
                $_SESSION['cart'][$product_id]['order_item_qty'] = $qty ;
        }
        
        die('{"success": true}');     
    }
    
    public function remove() {
        
        if(isset($_POST['id'])) {
            $product_id = (int)trim($this->input->post('id'));
            
            if(isset($_SESSION['cart'][$product_id])) {
                unset($_SESSION['cart'][$product_id]);
                echo '{"success": true}';
                die();
            }
        }

        echo '{"fail": true}';
        die();
    }
    
    public function clear() {
        if(isset($_SESSION['cart'])) {
            $_SESSION['cart'] = null;
        }
        
        redirect('cart', 'location', 302);
    }
    
    public function checkout() {
        if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
            
            $customer_name = htmlentities(trim($this->input->post('name')));
            $customer_address = htmlentities(trim($this->input->post('address')));
            $customer_contact = htmlentities(trim($this->input->post('contact')));
            
            
            if(empty($customer_name) || empty($customer_address) || empty($customer_contact)) {
                $_SESSION['checkoutError'] = 'Error! Please provide the valid customer info for delivery of this order';
                redirect('cart', 'location', 302);
            }
            
            
            $customer_data = [
                'customer_name' => $customer_name,
                'customer_address' => $customer_address,
                'customer_contact' => $customer_contact
            ];
            
            $this->load->model('orders_model');
            if($order_id = $this->orders_model->create_order($customer_data)) {
                foreach ($_SESSION['cart'] as $order_item) {
                    $item_data = [
                        'order_id' => $order_id,
                        'product_id' => $order_item['product_id'],
                        'order_item_qty' => $order_item['order_item_qty']
                    ];
                    
                    $this->orders_model->add_order_item($item_data);
                }
                
                $_SESSION['cart'] = null;
                $_SESSION['orderSuccess'] = $order_id;
                redirect('cart/success', 'location', 302);
            }
            else {
                $_SESSION['checkoutError'] = "Error occurred while processing your, please try again!";
            }
        }
        
        redirect('cart', 'location', 302);
    }
    
    
    public function success() {
        
        if(isset($_SESSION['orderSuccess'])) {
            $data['title'] = "Order Placed Successfully!";
            $this->load->view('templates/header', $data);
            $this->load->view('cart/success');
            $this->load->view('templates/footer');
        }
        else {
            redirect('cart', 'location', 302);
        }

    }
}
