<?php
class Categories extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('categories_model');
    }

    public function index()
    {
        $this->create();

    }

    
    public function create() {
        
        if(!isset($_SESSION['admin'])) {
            redirect('/', 'location', 302);
        }
        
        
        $data['categories'] = $this->categories_model->get_categories();

        $data['title'] = 'Category';
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category', 'Category Name', 'required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('categories/create');
            $this->load->view('templates/footer');
        }
        else
        {
            if($this->categories_model->set_category()) {
                $_SESSION['catSuccess'] = 'Category created successfully!';
            }
            else {
                $_SESSION['catFail'] = 'Creation Failed! Category may already exists.';
            }
            redirect('categories/create', 'location', 302);
        }
    }
}
