<div class="row">
    <h2 class="col-12"><?php echo $title; ?></h2>
    <hr class="col-12">
</div>

<div class="row">
    <?php foreach ($products as $products_item): ?>

        <div class=" col-md-6 col-lg-4 mb-4">
            <div class="card text-center">
                
                <a href="<?php echo site_url('products/'.$products_item['product_id']);?>">
                    <img class="img-fluid thumbnail"  src="<?php echo base_url('static/uploads/thumbnails/'.$products_item['product_img']) ?>" >
                </a>
                <div class="card-body ">
                    <h3 class="card-title">
                        <?php echo $products_item['product_name']; ?>
                    </h3>
                    <p class="card-text">
                        Price: $<?php echo $products_item['product_price']; ?>
                    </p>
                    
                </div>
                
                <div class="card-footer bg-dark text-white">
                    <button id="<?php echo $products_item['product_id']; ?>"
                            class="btn btn-primary btn-add-item">Add to cart</button>
                    
                </div>
            </div>
        </div>

    <?php endforeach; ?>
</div>
