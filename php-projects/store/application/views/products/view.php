



<div class="row justify-content-center">
    <div class="card text-center">
        <img class="img-fluid" src="<?php echo base_url('static/uploads/'.$products_item['product_img']) ?>" >

        <div class="card-body ">
            <h3 class="card-title">
                <?php echo $products_item['product_name']; ?>
            </h3>
            <p class="card-text">
                Price: $<?php echo $products_item['product_price']; ?>
                <hr>
                Category: <?php echo $products_item['category_name']; ?>
            </p>

        </div>

        <div class="card-footer bg-dark text-white">
            <button id="<?php echo $products_item['product_id']; ?>"
                    class="btn btn-primary btn-add-item">Add to cart</button>

        </div>
    </div>
</div>
