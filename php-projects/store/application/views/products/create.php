<div class="row">
    <h2 class="col-12"><?php echo $title; ?></h2>
    <hr class="col-12">
</div>

<?php echo validation_errors(); ?>



<?php if(isset($_SESSION['createSuccess'])) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <strong><?php echo $_SESSION['createSuccess'];?></strong>
    </div>
<?php endif;
$_SESSION['createSuccess'] = null;
?>


<?php if(isset($_SESSION['createError'])) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <strong><?php 
            foreach ($_SESSION['createError'] as $value) {
                echo $value .'<br>';
            }
        ?></strong>
    </div>
<?php endif; 
$_SESSION['createError'] = null;
?>




<div class="form-holder">
    <!--<form class="center-form" method="post">-->
    <?php echo form_open('products/create', array('class' => 'center-form', 'enctype' =>'multipart/form-data')); ?>

        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal form-title">Create a new product</h1>
        </div>

        <div class="form-label-group">
            <input name="name" type="text" id="inputName" class="form-control" placeholder="Product Name" value="" autofocus required>
            <label for="inputName">Product Name</label>
        </div>

        <div class="form-label-group">
            <input name="price" type="number" id="inputPrice" class="form-control" placeholder="Product Price" required>
            <label for="inputPrice">Product Price</label>
        </div>
    
        <div class="form-label-group"> 
            <input name="imgFile" type="file" id="inputImg" class="form-control" required>
            <label for="inputImg">Product Image</label>
        </div>
    
        <div class="form-label-group" > 
            <select name="category" id="inputCategory" class="form-control" required>
                <?php foreach ($categories as $category) :?>
                <option value="<?php echo $category['category_id']?>"><?php echo $category['category_name']?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <button name="submit" class="btn btn-lg btn-success btn-block mt-5" type="submit">Create Product</button>
    </form>
</div>