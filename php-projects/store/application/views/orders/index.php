<div class="row">
    <h2 class="col-12"><?php echo $title; ?></h2>
    <hr class="col-12">
</div>

<div class="row">
    <div class="col-12">
        <table class="table bg-dark text-white">
            <caption>Orders</caption>
            <thead>
                <tr>
                    <th scope="col" >Order#</th>
                    <th scope="col" >Status</th>
                    <th scope="col" >Customer Name</th>
                    <th scope="col" >Contact</th>
                    <th scope="col" >Address</th>
                    <th scope="col" >View Order</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($orders as $order): ?>
                <?php 
                $bg = '';
                if($order['order_status']) {
                     $bg = 'bg-success';
                } ?>
                <tr class="<?php echo $bg; ?>">
                    <th scope="row"><?php echo $order['order_id'] ?></th>
                    <td><?php echo $order['order_status'] == 1 ? 'Delivered' : 'Not Delivered' ?></td>
                    <td><?php echo $order['customer_name'] ?></td>
                    <td><?php echo $order['customer_contact'] ?></td>
                    <td><?php echo $order['customer_address'] ?></td>
                    <td><a href="<?php echo site_url('orders/'.$order['order_id']); ?>" class="btn btn-primary">View Order</a></td>
                </tr>
                <?php endforeach; ?>
                
            </tbody>
        </table>
        
    </div>
</div>
