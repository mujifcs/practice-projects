
<?php if(isset($_SESSION['success'])) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <strong><?php echo $_SESSION['success']; $_SESSION['success'] = null;   ?></strong>
    </div>
<?php endif; ?>



<div class="row">
    
    <div class="col-12">
        <?php $order = $order_items[0];
            if($order) :?>
        
            <h2>
                Order# <?php echo $order['order_id']; ?>
            </h2>
            <table class="table">
                <tbody>
                    <tr class="bg-dark text-white">
                        <th scope="row">Status</th>
                        <td>
                            <h4><?php echo $order['order_status'] == 1 ? 'Delivered' : 'Not Delivered' ?></h4>
                            <?php 
                                if($order['order_status'] == 0 && isset($_SESSION['admin']) ): ?>
                                    <a href="<?php echo site_url('orders/deliver/'.$order['order_id']); ?>" class="btn btn-lg btn-primary">Mark as Delivered</a>

                            <?php endif;?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" >Customer Name</th>
                        <td><?php echo $order['customer_name'] ?></td   >
                    </tr>
                    <tr>
                        <th scope="row" >Customer Contact</th>
                        <td><?php echo $order['customer_contact'] ?></td>
                    </tr>
                    <tr>
                        <th scope="row" >Customer Address</th>
                        <td><?php echo $order['customer_address'] ?></td>
                    </tr>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    
    <div class="col-12">
        <table class="table table-striped text-center">
            <thead class="thead-inverse">
                <tr class="bg-success text-white">
                <th scope="col">Order_item#</th>
                <th scope="col">Product Id</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product price per item ($)</th>
                <th scope="col">Ordered Quantity</th>
                <th scope="col">Item Total ($)</th>
              </tr>
            </thead>
            <tbody >
                <?php 
                $total = 0;
                foreach ($order_items as $order) : ?>
                <tr>
                  <th scope="row"><?php echo $order['order_item_id']?></th>
                  <td><?php echo $order['product_id']?></td>
                  <td><?php echo $order['product_name']?></td>
                  <td><?php echo $order['product_price']?></td>
                  <td><?php echo $order['order_item_qty']?></td>
                  <td><?php echo $item_total = $order['product_price'] * $order['order_item_qty']; $total += $item_total;?></td>
                </tr>
                <?php  endforeach; ?>
                
                <tr class="text-center bg-success text-white">
                    <th colspan="4">
                        Total
                    </th>
                    
                    <th colspan="2">
                        <?php echo $total ?>
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
</div>