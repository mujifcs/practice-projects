<div class="row">
    <h2 class="col-12"><?php echo $title; ?></h2>
    <hr class="col-12">
</div>

<?php echo validation_errors(); ?>



<?php if(isset($_SESSION['catSuccess'])) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <strong><?php echo $_SESSION['catSuccess'];?></strong>
    </div>
<?php endif; $_SESSION['catSuccess'] = null; ?>

<?php if(isset($_SESSION['catFail'])) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <strong><?php echo $_SESSION['catFail'];?></strong>
    </div>
<?php endif; $_SESSION['catFail'] = null?>



<div class="row">
    
    <div class="col-md-6 mb-3">
        <div class="form-holder">
            <!--<form class="center-form" method="post">-->
            <?php echo form_open('categories/create', array('class' => 'center-form')); ?>

                <div class="text-center mb-4">
                    <h1 class="h3 mb-3 font-weight-normal form-title">Create a new Category</h1>
                </div>

                <div class="form-label-group">
                    <input name="category" type="text" id="categoryName" class="form-control" placeholder="Category Name" value="" autofocus required>
                    <label for="categoryName">Product Name</label>
                </div>

                <button name="submit" class="btn btn-lg btn-success btn-block mt-5" type="submit">Create Category</button>
            </form>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="card text-center">
            <div class="card-header bg-dark text-white">
                <h3>
                    Categories
                </h3>
            </div>

            <div class="card-body bg-light">
                <ul class="list-group">
                    <?php foreach ($categories as $category) :?>
                    <li class="list-group-item" id="<?php echo $category['category_id']?>"><?php echo $category['category_name']?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    
</div>