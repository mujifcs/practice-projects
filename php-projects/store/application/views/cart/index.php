
<div class="row">
    <h2 class="col-12"><?php echo $title; ?></h2>
    <hr class="col-12">
</div>


<?php if(isset($_SESSION['checkoutError'])) : ?>

    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <strong><?php echo $_SESSION['checkoutError']; $_SESSION['checkoutError'] = null;?></strong>

    </div>

<?php endif; ?>


<div class="row">
    <div class="col-12">
        <table class="table table-striped text-center">
            <thead class="thead-inverse">
                <tr class="bg-success text-white">
                <th scope="col">item#</th>
                <th scope="col">Product Name</th>
                <th scope="col">Price Per Item ($)</th>
                <th scope="col">Ordered Quantity</th>
                <th scope="col">Item Total ($)</th>
                <th></th>
              </tr>
            </thead>
            <tbody >
                <?php 
                $total = 0;
                $i = 1;
                foreach ($cart_items as $cart_item) : ?>
                
                    <?php
                        // get quantity of prodcut from session acording to current prodcut
                        $order_item_qty = $_SESSION['cart'][$cart_item['product_id']]['order_item_qty'];
                    ?> 
                
                
                    <tr id="r-<?php echo $cart_item['product_id']; ?>">
                        
                        <th scope="row"><?php echo $i++; ?></th>
                        
                        <td><?php echo $cart_item['product_name']?></td>
                        
                        <td class="price"><?php echo $cart_item['product_price']?></td>
                        
                        <td><input type="number" min="1" class="form-control text-center qty" value="<?php echo $order_item_qty; ?>"></td>
                        
                        <td class="item-total"><?php echo $item_total = $cart_item['product_price'] * $order_item_qty; $total += $item_total;?></td>
                        
                        <td><button id="<?php echo $cart_item['product_id']; ?>" class="btn btn-danger btn-sm btn-remove-item">Remove item</button></td>
                    </tr>
                
                <?php  endforeach; ?>

                <tr class="text-center bg-success text-white">
                    <th colspan="4">
                        <h3>Grand Total</h3>
                    </th>

                    <th colspan="2">
                        <h3 id="total"><?php echo $total ?></h3>
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    
</div>


<div class="row">
    <?php if(!empty($_SESSION['cart'])) :?>
    
        <div class="col-12">
            <button id="btn-checkout" class="btn  btn-primary float-right btn ">Check Out ></button>
            <a href="<?php echo site_url('cart/clear') ?>" class="btn  btn-danger float-left">Delete Cart</a>
        </div>
    
    <?php else : ?>

        <div class="col-12 text-center">
            <h3>Cart is empty!</h3>
        </div>

    <?php endif; ?>
</div>





