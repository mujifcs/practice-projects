
<div class="row">
    
    <div class="col-12">
        <div class="card text-center">
            <div class="card-header bg-dark text-white">
                <h3>
                    <h2 class="col-12">Order# <?php echo $_SESSION['orderSuccess']; ?></h2>
                </h3>
            </div>

            <div class="card-body ">
                <a href="<?php echo site_url('orders/'.$_SESSION['orderSuccess']); ?>" class="btn btn-primary">View Order</a>
            </div>
        </div>
    </div>
</div>