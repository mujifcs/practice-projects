<div class="modal fade" id="checkoutModal" tabindex="-1" role="dialog" aria-labelledby="checkoutModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">    
        <h5 class="modal-title" id="checkoutModalLabel">Order Placement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
      <div class="modal-body">
        <div class="form-holder">
            <form class="center-form" method="post" action="<?php echo site_url('cart/checkout');?>">

                <div class="text-center mb-4">
                    <h1 class="h3 mb-3 font-weight-normal form-title">Customer Information</h1>
                </div>

                
                <div class="form-label-group">
                    <input name="name" type="text" id="inputName" class="form-control" placeholder="Customer Name" value="<?= $email ?? '' ?>" required autofocus>
                    <label for="inputName">Customer Name</label>
                </div>
                
                <div class="form-label-group">
                    <input name="address" type="text" id="inputAddr" class="form-control" placeholder="Delivery Address" value="<?= $email ?? '' ?>" required>
                    <label for="inputAddr">Delivery Address</label>
                </div>

                <div class="form-label-group">
                    <input name="contact" type="tel" id="inputContact" class="form-control" placeholder="Contact Number i.e. 03001234567" required>
                    <label for="inputContact">Contact Number</label>
                </div>

                <button name="submit" class="btn btn-lg btn-success btn-block mt-5" type="submit">Purchase</button>
            </form>
        </div>
      </div>
        
      
    </div>
  </div>
</div>