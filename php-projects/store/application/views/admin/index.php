


<?php
    if(isset($_SESSION['loginError'])) : ?>

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong>Login error!! <?php echo $_SESSION['loginError']; $_SESSION['loginError'] = null;?></strong>
            
        </div>

    <?php endif;
?>



<div class="form-holder">
    <form class="center-form" method="post">

        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal form-title">Log In to manage The Store</h1>
        </div>

        <div class="form-label-group">
            <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" value="<?= $email ?? '' ?>" required autofocus>
            <label for="inputEmail">Email address</label>
        </div>

        <div class="form-label-group">
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <label for="inputPassword">Password</label>
        </div>

        <button name="submit" class="btn btn-lg btn-success btn-block mt-5" type="submit">Log In</button>
    </form>
</div>