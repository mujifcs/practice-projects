<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imageprocess
{
    protected $permittedTypes = array(
                    'image/jpeg',
                    'image/jpg',
                    'image/png'
    );

    public function __construct()
    {

    }

    public function getImageInfo($filepath) {
        return getimagesize($filepath);
    }

    public function imageCreateFromFile($filepath, $imageType) {

        switch($imageType) {
            case 'image/png'  : return imagecreatefrompng($filepath);

            case 'image/jpeg' :
            case 'image/jpg'  : return imagecreatefromjpeg($filepath);
            default             : return false;
        }
    }
    
    
    
    public function createWatermark($im){
        $markW = 200;
        $markH = 100;

        $stamp = imagecreatetruecolor($markW, $markH);
        imagefilledrectangle($stamp, 0, 0, $markH, $markW, 0x000000);
        imagestring($stamp, 5, 20, 40, "(c) eStore", 0xFFFFFF);

        // Set the margins for the stamp and get the height/width of the stamp image
        $marge_right = 40;
        $marge_bottom = 40;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);

        // Merge the stamp onto our photo with an opacity of 50%
        imagecopymerge(
                $im, $stamp,
                imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom,
                0, 0,
                $sx, $sy,
                70
            );

        return $im;
    }


    public function processImage($filepath, $type,  $name, $img_dir, $thumbnail_dir) {

        $result = false;
        
        $image = $this->imageCreateFromFile($filepath, $type);
        
        if($image) {
            
            $watermarkImg = $this->createWatermark($image);

            if($watermarkImg) {
                
                $thumb = imagescale($watermarkImg, 300);

                if($thumb) {
                    
                    if($this->saveImage($thumb, $thumbnail_dir.$name, $type)) {
                        
                        if($this->saveImage($watermarkImg, $img_dir.$name, $type)) {
                            $result = true;
                        }
                    }
                    
                    imagedestroy($thumb);
                }
                imagedestroy($watermarkImg);
            }
        }
        
        return $result;
    }
    
    
    public function saveImage($im, $path , $imageType) {
        
        switch($imageType) {
            case 'image/png'  : return imagepng($im, $path);

            case 'image/jpeg' :
            case 'image/jpg'  : return imagejpeg($im, $path);
            default             : return false;
        }
    }
}