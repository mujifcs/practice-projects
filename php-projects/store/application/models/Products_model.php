<?php
class Products_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }


    public function get_products($product_id = FALSE, $where_in_data = FALSE)
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->join('products', 'categories.category_id = products.category_id');
        
        if ($product_id === FALSE)
        {
            $query = $this->db->get();
            return $query->result_array();
        }
        
        if($where_in_data === FALSE) {
            $this->db->where("product_id = $product_id");
            $query = $this->db->get();
            return $query->row_array();
        }
        
        $this->db->where_in('product_id', $where_in_data);
        $query = $this->db->get();
        return $query->result_array();
        
    }
    
    
    public function set_product($data) {
        $this->db->insert('products', $data);
        return $this->db->affected_rows() > 0;
    }
}