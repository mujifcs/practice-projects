<?php
class Orders_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }


    public function get_orders($order_id = FALSE)
    {
        if ($order_id === FALSE)
        {
            $query = $this->db->get('orders');
            return $query->result_array();
        }
        
        
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->join('order_items', 'orders.order_id = order_items.order_id');
        $this->db->join('products',  'order_items.product_id = products.product_id');
        $this->db->where("orders.order_id = $order_id");

        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    public function update_status($order_id, $order_status = 0) {
        $data = [
            'order_status' => $order_status 
        ];
        
        $this->db->where('order_id', $order_id);
        $this->db->update('orders', $data);
    }
    
    public function create_order($customer_data) {
        $this->db->insert('orders',$customer_data);
        return $this->db->insert_id();
    }
    
    public function add_order_item( $item_data) {
        $this->db->insert('order_items',$item_data);
        return true;
    }
}