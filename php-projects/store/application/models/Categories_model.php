<?php
class Categories_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }


    public function get_categories($category_name = false)
    {
        if($category_name === false) {
            $query = $this->db->get('categories');
            $this->db->order_by("category_name", "ASC");
            return $query->result_array();
        }
        
        $query = $this->db->get_where('categories', array("category_name" => $category_name));
        return $query->row_array();
    }
    
    
    public function set_category() {
        
        $category_name = ucwords(strtolower(trim($this->input->post('category'))));
        
        $result = $this->get_categories($category_name);
        
        if($result) {
            return false;
        }
        
        
        $data = array(
            'category_name' => $category_name
        );
        
        return $this->db->insert('categories', $data);
    }
}