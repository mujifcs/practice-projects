<?php

class Connection
{

    private static $username = 'root';
    private static $password = '';

    // Data Source Name (DSN)
    private static $dsn = 'mysql:host=localhost;dbname=imagesgallery;charset=utf8mb4';

    private static $options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];


    public static function make()
    {
        try {
            return new PDO(self::$dsn, self::$username, self::$password, self::$options);
        }
        catch(Exception $e) {
            showErrorPage(500);
        }
    }
}
