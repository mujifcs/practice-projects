<?php


class QueryBuilder
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    

    public function selectAll($table, $selection = '*', $orderby='id ASC')
    {
            $stmt = $this->pdo->prepare("SELECT $selection FROM $table ORDER BY $orderby");
            $stmt->execute();
            return $stmt;
    }
    
    public function insert($table, $params) {
            $sql = sprintf(
                'INSERT INTO %s (%s) values (%s)',
                $table,
                implode(', ', array_keys($params)),
                ':'.implode(', :', array_keys($params))
            );
            
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($params);
            return $stmt;
    }
      
    public function seleclConditional($table, $params, $selection = '*') {
            
            $sql = "SELECT $selection FROM $table WHERE ";
            
            $keys = array_keys($params);
   
            for($i =0; $i< count($keys); $i++) {
                
                $cond = "$keys[$i] = :$keys[$i]";
                
                if($i!= count($keys) - 1) {
                    $cond .= " AND ";
                }
                
                $sql .= $cond;
            }  
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($params);
            return $stmt;       
    }
}