<?php

///  datbase fun
function  updatePassword($id, $pass, $pdo) {
    $stmt = $pdo->prepare("UPDATE users SET password = ? WHERE id = $id");
    $stmt->execute([$pass]);
    return $stmt;
}


function selectAllImagesAndRating($pdo, $orderbyValue = "images.id DESC") {
    $sql = "SELECT images.id, images.imgname, AVG(ratings.rating) as avg "
            . "FROM images LEFT JOIN ratings ON images.id = ratings.imgid "
            . "GROUP By images.id "
            . "ORDER BY $orderbyValue";
    
    return $pdo->query($sql);
}