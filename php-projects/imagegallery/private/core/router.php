<?php

$routes =  [

    '' => getPathFor('controllers/index.php'),
    'signup' => getPathFor('controllers/signup.php'),
    'login' => getPathFor('controllers/login.php'),
    'logout' => getPathFor('controllers/logout.php'),
    'profile' => getPathFor('controllers/profile.php'),
    'password' => getPathFor('controllers/password.php'),
    'imageupload' => getPathFor('controllers/imageupload.php'),
    'image' => getPathFor('controllers/image.php'),
    'rating' => getPathFor('controllers/ratingprocessing.php'),
    'toprated' => getPathFor('controllers/toprated.php')
    
];


$titles = [
    '' => 'Recent Images',
    'signup' => 'Sign Up',
    'login' => 'Log In',
    'profile' => 'Profile View',
    'password' => 'Password change',
    'imageupload' => 'Image Uplaod',
    'image' => 'Image view',
    'toprated' => 'Top Rated Images'
];


function getTitle($path)
{
    global $titles;
    
    if(key_exists($path, $titles)) {
        return $titles[$path];
    }
    return null;
}


function getController($path) 
{
    global $routes;
    
    if(key_exists($path, $routes)) {
       return $routes[$path];
    }
     else {
        showErrorPage(404);
    }

}

function showErrorPage($errorCode)
{
    http_response_code($errorCode);
    require getPathFor('views/error.view.php');
    die();
}