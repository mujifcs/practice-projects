
<?php


// retrun path to private file
function getPathFor($path) {
    return PRIVATE_PATH . $path;
}


// return path of resourse from url
function getPath() {
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $path = strtolower(trim($path,'/'));
    return $path;
}

function getRequestType(){
    return $_SERVER['REQUEST_METHOD'];
}

function redirect($path) {
    header("location: $path");
    die();
}


function checkLogin() {
    if(isset($_SESSION['uid'])){
        redirect("/");
    }
}





/// form validation function
function isPresent($value) {
    return isset($value) && trim($value) !== '';
}

function isLow($value, $minLen) {
    if(strlen($value) < $minLen) {
        return true;
    }
    return false;
}


function isHigh($value, $maxLen) {
    if(strlen($value) > $maxLen) {
        return true;
    }
    return false;
}



// validate name
function validateName($filedname, $messageName, &$errors, $value, $minLen = 2, $maxLen = 20) {
    if(isPresent($value)) {
        $validatedValue= trim($value);
        
        if(isLow($validatedValue, $minLen)  || isHigh($validatedValue, $maxLen)) {
            $errors[$filedname] = "$messageName lenght must be between $minLen and $maxLen";
        }
    }
    else {
        $errors[$filedname] = "$messageName name cannot be empty!";
    }
    
    return $validatedValue ?? '';
}


// validate password
function validatePassword($password, &$errors, $minLen = 8, $maxLen = 50) {
    if(isPresent($password)) {
        $validatedPassword= trim($password);
        
        if(isLow($validatedPassword, $minLen)  || isHigh($validatedPassword, $maxLen)) {
            $errors['password'] = "Password lenght must be between $minLen and $maxLen";
        }
    }
    else {
        $errors['password'] = 'Password cannot be empty!';
    }
    
    return $validatedPassword ?? '';
}


// validate email 
function validateEmail($email,  &$errors, $maxLen = 30) {

    if(isPresent($email)) {
        $validatedEmail= trim($email);
        
        if(isHigh($validatedEmail, $maxLen)) {
            $errors['email'] = "email lenght must be less than $maxLen";
        }
        
        if(!filter_var($validatedEmail, FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Please provide a valid email';
        }
    }
    else {
        $errors['email'] = 'email cannot be empty!';
    }
    return $validatedEmail ?? '';
}

function sanatizeHtml($string) {
    return htmlentities($string);
}




// image processing functions
function createWatermark($im){
    $markW = 200;
    $markH = 100;

    $stamp = imagecreatetruecolor($markW, $markH);
    imagefilledrectangle($watermark, 0, 0, $markH, $markW, 0x000000);
    imagestring($stamp, 5, 20, 40, "(c) Image Gallery", 0xFFFFFF);
    
    // Set the margins for the stamp and get the height/width of the stamp image
    $marge_right = 40;
    $marge_bottom = 40;
    $sx = imagesx($stamp);
    $sy = imagesy($stamp);
    
    // Merge the stamp onto our photo with an opacity of 50%
    imagecopymerge(
            $im, $stamp,
            imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom,
            0, 0,
            $sx, $sy,
            70
        );
    
    return $im;
}


function processImage($filepath, $type,  $name, $img_dir, $thumbnail_dir) {
        
    $result = false;
    
    switch ($type) {
        case 'image/png':
            $image = imageCreateFromPng($filepath);
            if($image) {
                $watermarkImg = createWatermark($image);
                if($watermarkImg) {
                    $thumb = imagescale($image, 400);
                    if($thumb) {
                        if(imagepng($thumb, $thumbnail_dir.$name)) {
                            if(imagepng($image, $img_dir.$name)) {
                                $result = true;
                            }
                        }
                        imagedestroy($thumb);
                    }
                    imagedestroy($watermarkImg);
                }
                
                imagedestroy($image);
            }
        break;
            
        case 'image/jpg':
        case 'image/jpeg':
            $image = imageCreateFromJpeg($filepath);
            if($image) {
                $watermarkImg = createWatermark($image);
                if($watermarkImg) {
                    $thumb = imagescale($image, 400);
                    if($thumb) {
                        if(imageJpeg($thumb, $thumbnail_dir.$name)) {
                            if(imageJpeg($image, $img_dir.$name)) {
                                $result = true;
                            }
                        }
                        imagedestroy($thumb);
                    }
                    imagedestroy($watermarkImg);
                }
                imagedestroy($image);
            }
        break;
    
    }

    return $result;
}



