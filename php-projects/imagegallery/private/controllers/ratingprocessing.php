<?php

if( !isset($_POST['id']) || !isset($_POST['rating']) ||!isset($_SESSION['uid'])  ) {
    echo '{"fail": true}';
    die();
}

$imgid = (int)trim($_POST['id']);

$rating = (float) trim($_POST['rating']);


try {
    $stmt = $queryBuilder->insert('ratings', ['uid' => $_SESSION['uid'], 'imgid' => $imgid, 'rating' => $rating]);
    
    if($stmt->rowCount() > 0) {
        
        $success = [];
        $success['success'] = true;
        
        $success['userRating'] = $rating;
        
        $stmt= null;
        
        $stmt = $queryBuilder->seleclConditional('ratings', ['imgid'=>$imgid ], 'avg(rating) as avg, count(*) as totalRatings');
        
        if($stmt->rowCount() > 0) {
            
            $res = $stmt->fetch();
            $stmt =null;
            
            $success['totalRatings'] = $res['totalRatings'];
            $success['avg'] = round($res['avg'], 1);
            
            $res = null;
        }
        
        
        echo json_encode($success);
    }
    else {
        echo '{"fail": true}';
    }
    
} catch (Exception $exc) {
    echo '{"fail": true}';
}
