<?php

    // if not logged in
    if(!isset($_SESSION['uid'])){
        redirect("/");
    }
    
    if(isset($_POST['submit'])) {
        $oldPassword = '';
        $newPassword = '';

        $errors  = [];

        $oldPassword = validatePassword($_POST['oldPassword'], $errors);  
        if(isset($errors['password'])) {
            $errors['oldPassword'] = "Old ".$errors['password'];
            unset($errors['password']);
        }
        
        $newPassword = validatePassword($_POST['newPassword'], $errors);
        if(isset($errors['password'])) {
            $errors['newPassword'] = "New ".$errors['password'];
            unset($errors['password']);
        }
        
        if(empty($errors)) {
            try {
                $res = $queryBuilder->seleclConditional('users', ['id' => $_SESSION['uid']]);               
                $user = $res->fetch();
                $res = null;
                
                if(password_verify($oldPassword, $user['password'])) {
                    $hash = password_hash($newPassword, PASSWORD_BCRYPT);
                     
                    $res = updatePassword($_SESSION['uid'], $hash, $pdo);
                    if($res->rowCount()) {
                        $_SESSION['passwordSuccess'] = 'Password changed successfully';
                        redirect("/password");
                    }
                    else {
                         $errors[] = "password Chnage failed!";
                    }
                    $res = null;
                }
                else {
                    $errors[] = "Old password mismatched!";
                }
            } catch (Exception $exc) {
                $errors[]= "Something bad happened at our side, Please try again.";
            }
            
        }
        
        if(!empty($errors)) {
            $_SESSION['passwordErrors'] = $errors;
            redirect("/password");
        }
        
    }
    
    require getPathFor('views/password.view.php');