<?php

    // if already logged in send to home
    checkLogin();

    if(isset($_POST['submit'])) {
        
        $email = '';
        $password = '';

        $errors  = [];
          
        $password = validatePassword($_POST['password'], $errors);
        
        $email = validateEmail($_POST['email'], $errors);
        
 
        if(empty($errors)) {
            try {
                $res = $queryBuilder->seleclConditional('users', ['email' => $email]);
                    
                // if registered
                if($res->rowCount() > 0) {
                                     
                    $user = $res->fetch();
                    $res = null;
                    
                    if(password_verify($password, $user['password'])) {
                        // login
                        $_SESSION['uid'] = $user['id'];
                        $_SESSION['name'] = $user['fname'];
                    
                        redirect('/');
                    }
                    else {
                        $errors['email'] = "Email or Password invalid";  // basically password is invalid 
                    }
                }
                else {
                    $errors['email'] = 'Account not Registered!';
                }
                
            } catch (Exception $ex) {
                $errors = "Something bad happened at our side, Please try again.";
            }
        }
        else {
            $errors = array();
            $errors['email'] = "Email or Password invalid";
        }
        
        $_SESSION['loginValues'] = [
            'errors' => $errors,
            'email' => $email
        ];
        redirect('/login');
        
    }
    
    
    // if GET
    require getPathFor('views/login.view.php');