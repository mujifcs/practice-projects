<?php


    // if not logged in
    if(!isset($_SESSION['uid'])){
        redirect("/");
    }
    
    if(isset($_POST['submit'])) {
        
        $filepath = $_FILES['imgFile']['tmp_name'];
        $size = $_FILES['imgFile']['size'];
        $maxSize = 2097152;
        
        if( $_FILES['imgFile']['error'] > 0 ) {
            $errors[] = 'Error in getting the file due to file type/size';
        }
        else {
            $imginfo = getimagesize($filepath);

            if($imginfo !== false) {

                $ext = explode('/', $imginfo['mime'])[1];
                $name = "i".$_SESSION['uid'] . time() . ".$ext";
                $img_dir = "uploads/";
                $thumbnail_dir = "uploads/thumbnails/";
                
                
                $result = processImage($filepath, $imginfo['mime'], $name, $img_dir, $thumbnail_dir);
                if($result) {
                    
                    try {
                        $stmt = $queryBuilder->insert('images', ['uid' => $_SESSION['uid'], 'imgname' => $name]);
                        if($stmt->rowCount() > 0) {
                            $_SESSION['uploadSuccess'] = "Image Uploaded successfully";
                            redirect("imageupload");
                        }
                        else {
                            $errors[] = "We are unable to perform your action, please try later";
                        }
                        $stmt = null;
                    } catch (Exception $ex) {
                        showErrorPage(500);
                    }
                }
                else {
                    $errors[] = "Error handling the image, only jpej/jpg, png allowed, please try agian";
                }
            }
            else {
                $errors[] = "Not an image file";
            }
        }

        $_SESSION['uploadFail'] = $errors;
        redirect("imageupload");
        
    }
    
    // if get request
    require getPathFor('views/imageupload.view.php');
