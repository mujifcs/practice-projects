<?php


if(!isset($_GET['id'])) {
    redirect('/');
}

$id = trim($_GET['id']);

try{
    $isRatingAllowed = false;
    $image = $queryBuilder->seleclConditional('images', ['id'=> $id]);
    
    if($image->rowCount() < 1) {
        showErrorPage(404);
    }
    
    $img = $image->fetch();
    $image = null;
    
    $rating = $queryBuilder->seleclConditional('ratings', ['imgid'=>$img['id']], 'avg(rating) as avg, count(*) as totalRatings');
    
    
    if($rating->rowCount() > 0) {
        $ratingData = $rating->fetch();
        
        // if user loggedin check eligibilty to rate this image
        if(isset($_SESSION['uid'])) {
            $userRating = $queryBuilder->seleclConditional('ratings', ['imgid'=>$img['id'], 'uid' => $_SESSION['uid']]);
            
            if($userRating->rowCount() < 1) {
                $isRatingAllowed = true;
            }
            else {
                $myRating = $userRating->fetch();
                $myRating = round($myRating['rating'], 2);
            }   
        }
    }
    $rating = null;
    $userRating= null;

        
} catch (Exception $ex) {
    showErrorPage(500);
}

require getPathFor('views/image.view.php');