<?php

    // if not logged in
    if(!isset($_SESSION['uid'])){
        redirect("/");
    }
    
    try {
        $res = $queryBuilder->seleclConditional('users', ['id' => $_SESSION['uid']]);               
        $user = $res->fetch();
        $res = null;
        
    } catch (Exception $ex) {
        showErrorPage(500); 
    }
    
    
    require getPathFor('views/profile.view.php');
