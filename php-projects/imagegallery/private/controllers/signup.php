<?php

    // if already logged in send to home
    checkLogin();
 
    if(isset($_POST['submit'])) {
        
        $fname = '';
        $lname = '';
        $email = '';
        $password = '';

        $errors  = [];
    
        
        $fname = validateName('fname', 'First Name', $errors, $_POST['fname']);
        
        $lname = validateName('lname', 'Last Name', $errors, $_POST['lname']);
                
        $password = validatePassword($_POST['password'], $errors);
        
        $email = validateEmail($_POST['email'], $errors);
        
        if(!isset($errors['email'])) {
            try {
                $res = $queryBuilder->seleclConditional('users', ['email' => $email], 'count(*)');
                // already registered
                if($res->fetchColumn() > 0) {
                    $errors['email'] = 'Email is already registered!';
                }   
            } catch (Exception $ex) {
                 $errors = "Something bad happened at our side, Please try again or contact webmaster.";
            }
        }
        
        
        // procceed to sign up if no errors
        if(empty($errors)) {
            
            $hash = password_hash($password,PASSWORD_BCRYPT);
            
            $insertArray = [
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'password' => $hash
            ];
            
            try {
                $res = $queryBuilder->insert('users', $insertArray);
                if($res->rowCount() > 0){
                    $id = $pdo->lastInsertId();
                    
                    $_SESSION['uid'] = $id;
                    $_SESSION['name'] = $fname;
                    
                    redirect('/');
                }
                else {
                    $errors = "Something bad happened at our side, Please try again or contact webmaster.";
                }
            } catch (Exception $ex) {
                $errors = "Something bad happened at our side, Please try again.";
            }
        }
        
        $_SESSION['signupValues'] = [
            'errors' => $errors,
            'fname' =>  $fname,
            'lname' =>  $lname,
            'email' => $email
        ];
        redirect('/signup'); 
    }
    
    
    
    
    
    
    
    // if GET

    require getPathFor('views/signup.view.php');