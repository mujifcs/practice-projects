<?php
    require getPathFor('views/partials/header.php');
?>

<?php
    if(isset($_SESSION['signupValues'])) : ?>

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong>Sign up error!!</strong>

            <ul>
                <?php 
                  foreach ($_SESSION['signupValues']['errors'] as  $value) {
                      echo "<li> $value </li>";
                  }
                ?>
            </ul>
        </div>

        <?php
            $fname = sanatizeHtml($_SESSION['signupValues']['fname']);
            $lname = sanatizeHtml($_SESSION['signupValues']['lname']);
            $email = sanatizeHtml($_SESSION['signupValues']['email']);

            $_SESSION['signupValues']= null;
        ?>
    <?php endif;
?>


<div class="form-holder">
    <form class="form-login-signup" method="post">

        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal form-title">Create a new account</h1>
        </div>

        <div class="form-label-group">
            <input name="fname" type="text" id="inputFname" class="form-control" placeholder="First Name" value="<?= $fname ?? '' ?>"required autofocus>
            <label for="inputFname">First Name</label>
        </div>

        <div class="form-label-group">
            <input name="lname" type="text" id="inputLname" class="form-control" placeholder="Last Name" value="<?= $lname ?? '' ?>"required>
            <label for="inputLname">Last Name</label>
        </div>

        <div class="form-label-group">
            <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" value="<?= $email ?? '' ?>"required>
            <label for="inputEmail">Email address</label>
        </div>

        <div class="form-label-group">
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <label for="inputPassword">Password</label>
        </div>

        <button name="submit" class="btn btn-lg btn-primary btn-block mt-5" type="submit">Sign Up</button>
    </form>
</div>



<?php
	require getPathFor('views/partials/footer.php');
?>