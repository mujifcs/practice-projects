<?php
	require getPathFor('views/partials/header.php');
?>

<div class="row justify-content-center">
    <div class="col-md-7">
        <div class="card text-center">
            <img class="card-img-top" src="http://via.placeholder.com/450x250" alt="Card image cap">
            <div class="card-body">
                <h2 class="card-title"><?php echo sanatizeHtml($user['fname']) .' '. sanatizeHtml($user['lname']);?></h2>
                <p class="card-text"><?php echo sanatizeHtml($user['email']); ?></p>
                <a class="btn btn-primary" href="password" >Change Password</a>
                
            </div>
        </div>
    </div>  
</div>


<?php
	require getPathFor('views/partials/footer.php');
?>
