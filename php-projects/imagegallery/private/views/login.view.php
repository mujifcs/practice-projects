<?php
	require getPathFor('views/partials/header.php');
?>


<?php
    if(isset($_SESSION['loginValues'])) : ?>

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong>Login error!!</strong>

            <ul>
                <?php 
                  foreach ($_SESSION['loginValues']['errors'] as  $value) {
                      echo "<li> $value </li>";
                  }
                ?>
            </ul>
        </div>

        <?php
            $email = sanatizeHtml($_SESSION['loginValues']['email']);

            $_SESSION['loginValues']= null;
        ?>
    <?php endif;
?>



<div class="form-holder">
    <form class="form-login-signup" method="post">

        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal form-title">Log In to get started</h1>
        </div>

        <div class="form-label-group">
            <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" value="<?= $email ?? '' ?>" required autofocus>
            <label for="inputEmail">Email address</label>
        </div>

        <div class="form-label-group">
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <label for="inputPassword">Password</label>
        </div>

        <button name="submit" class="btn btn-lg btn-success btn-block mt-5" type="submit">Log In</button>
    </form>
</div>



<?php
	require getPathFor('views/partials/footer.php');
?>