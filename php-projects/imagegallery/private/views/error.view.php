<?php
	require getPathFor('views/partials/header.php');
?>

<div class="center-message">
	<h1>Oops! Something went wrong!</h1>
	<p class="lead">Error <?= $errorCode ?></p>
</div>

<?php
	require getPathFor('views/partials/footer.php');
?>