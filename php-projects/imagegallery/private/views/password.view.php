<?php
	require getPathFor('views/partials/header.php');
?>


<?php

    if(isset($_SESSION['passwordSuccess'])) : ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong><?php echo $_SESSION['passwordSuccess']; $_SESSION['passwordSuccess'] = null;?></strong>
        </div>
    <?php endif;



?>





<?php
    if(isset($_SESSION['passwordErrors'])) : ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong>Password Change error!!</strong>

            <ul>
                <?php 
                  foreach ($_SESSION['passwordErrors']   as  $value) {
                      echo "<li> $value </li>";
                  }
                  
                  $_SESSION['passwordErrors'] = null;
                ?>
            </ul>
        </div>
    <?php endif;
?>



<div class="form-holder">
    <form class="form-login-signup" method="post">

        <div class="text-center mb-4 ">
            <h1 class="h3 mb-3 font-weight-normal form-title">Change Your Password </h1>
        </div>

        <div class="form-label-group">
            <input name="oldPassword" type="password" id="oldPassword" class="form-control" placeholder="Current password" required>
            <label for="oldPassword">Current password</label>
        </div>
        
        <div class="form-label-group">
            <input name="newPassword" type="password" id="newPassword" class="form-control" placeholder="New password" required>
            <label for="newPassword">New password</label>
        </div>
        

        <button name="submit" class="btn btn-lg btn-primary btn-block mt-5" type="submit">Change password</button>
    </form>
</div>


<?php
	require getPathFor('views/partials/footer.php');
?>