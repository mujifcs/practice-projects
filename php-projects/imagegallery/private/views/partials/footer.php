
    	</main>
    	<!-- /.container -->




		<footer class="footer text-center mt-4">
			<div class="container">
				<span class="text-muted">&copy; <?= date('Y') ?> Image Gallery Inc. </span>
			</div>
		</footer>





		<!-- Bootstrap core JavaScript-->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
                
                <!-- Rate Yo Latest compiled and minified JavaScript -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
                
                
                
                <script>
                    //Make sure that the dom is ready
                    $(function () {

                      $(".rating").rateYo({
                        readOnly: true
                      });
                      
                      <?php if(isset($isRatingAllowed) && $isRatingAllowed == true) { ?>
                            $(".edit-rating").rateYo({
                                readOnly: false,
                                onSet: function (rating, rateYoInstance) {
                                    $(this).rateYo("option", "readOnly", true);
                                    var id = $(this).attr('id');
                                    id = id.split("-");
                                    
                                    var error;
                                    
                                    $.post( "rating", { id : id[1], rating: rating})
                                        .done(function(data) {
                                            
                                            data = jQuery.parseJSON(data);
                                            if(data['success']) {
                                               $('.card-title').html("You rated "+data['userRating']+" stars");
                                               $('#totalRatings').html(data['totalRatings']);
                                               $('#avg').html(data['avg']);
                                               $(".edit-rating").rateYo("option", "rating", data['avg']);   
                                            }
                                            else {
                                                error = true;
                                            }
                                        })
                                        .fail(function() {
                                           error = true;
                                        })
                                        .always(function() {
                                           if(error){
                                             $('.card-title').html("Rating error! please try again!");
                                             $(".edit-rating").rateYo("option", "readOnly", false);
                                           }
                                        })
                                        ;
                                }
                             });
                      <?php  } ?>
                      
                    });
                </script>    
	</body>
</html>