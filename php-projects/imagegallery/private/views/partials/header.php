<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="assest/favicon.ico"> -->

        <title>Image Gallery <?php if($title) { echo '| '.$title; } ?></title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        
        <!-- Rate YO Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

        
        <!-- Custom styles -->
        <link href="assests/style.css" rel="stylesheet">
    </head>

    <body>

        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">Image Gallery</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">

                <ul class="navbar-nav mr-auto">
                    <li class="nav-item <?php if($path == 'toprated') { echo 'active'; } ?>">
                        <a class="nav-link" href="toprated">Top Rated</a>
                    </li>
                    <?php if(isset($_SESSION['uid'])) : ?>
                        <li class="nav-item <?php if($path == 'imageupload') { echo 'active'; } ?>">
                            <a class="nav-link" href="imageupload">Upload Image</a>
                        </li>
                    <?php endif; ?>
                </ul> 

                <ul class="navbar-nav ml-auto">
                    <?php if(isset($_SESSION['uid'])) : ?>
                        <li class="nav-item dropdown ">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo sanatizeHtml($_SESSION['name']); ?></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
                                <a class="dropdown-item " href="profile">Profile</a>
                                <a class="dropdown-item" href="password" >Change Password</a>
                                <a class="dropdown-item" href="logout">Logout</a>
                            </div>
                        </li>

                    <?php else : ?>
                        <li class="nav-item <?php if($path == 'login') { echo 'active'; } ?>">
                                <a class="nav-link" href="login">Login</a>
                        </li>
                        <li class="nav-item <?php if($path == 'signup') { echo 'active'; } ?>">
                                <a class="nav-link" href="signup">Sign Up</a>
                        </li>
                    <?php endif; ?>
                </ul>

            </div>
        </nav>

        <main role="main" class="container">
            
            <div class="row">
                <h2 class="col-12">
                    <?php echo $title ?? ''; ?>
                </h2>
                <hr class="col-12">
            </div>
