<?php
	require getPathFor('views/partials/header.php');
?>

<?php

    if(isset($_SESSION['uploadSuccess'])) : ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong><?php echo $_SESSION['uploadSuccess']; $_SESSION['uploadSuccess'] = null;?></strong>
        </div>
    <?php endif;
?>


<?php
    if(isset($_SESSION['uploadFail'])) : ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong>Uploading failed!</strong>

            <ul>
                <?php 
                  foreach ($_SESSION['uploadFail']   as  $value) {
                      echo "<li> $value </li>";
                  }
                  
                  $_SESSION['uploadFail'] = null;
                ?>
            </ul>
        </div>
    <?php endif;
?>


<div class="form-holder">
    <form class="form-login-signup" enctype="multipart/form-data" method="post">

        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal form-title">Select image file to upload</h1>
        </div>
        
        <div class="form-label-group">  
            <input name="imgFile" type="file" id="inputPassword" class="form-control" required>
            <label for="inputPassword">Image file</label>
        </div>

        <button name="submit" class="btn btn-lg btn-primary btn-block mt-5" type="submit">Upload</button>
    </form>
</div>


<?php
	require getPathFor('views/partials/footer.php');
?>