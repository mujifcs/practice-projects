<?php
	require getPathFor('views/partials/header.php');
?>

<div class="row justify-content-center">
    <div class="col-lg-10">
        <div class="card text-center">
            <img class="card-img-top"src="/uploads/<?php echo $img['imgname']; ?>" alt="image">
            <div class="card-body">
                
                <?php
                
                    $class = 'rating';
                    
                    if(isset($myRating)) {
                        echo "<h4 class='card-title'>You rated $myRating stars</h4>";
                    }
                    else {
                        $msg = "";
                        if($isRatingAllowed) {
                            $msg = 'Rate the Image';
                            $class= 'edit-rating';
                        }
                        echo "<h4 class='card-title'>$msg</h4>";
                    }
                ?>
                
                <div class="<?php echo $class;?> card-text" 
                     id="<?php echo 'i-'.$img['id']; ?>"
                     data-rateyo-rating="<?php echo $ratingData['avg'] ?? 0; ?>" 
                ></div>
                
                <p class="small mt-1"><span id = "totalRatings"><?php echo $ratingData['totalRatings'] ?? 0; ?></span> people rated this image an average of <span id="avg"><?php echo round($ratingData['avg'], 1) ?? 0; ?></span></p>
            </div>
        </div>
    </div>  
</div>


<?php
	require getPathFor('views/partials/footer.php');
?>