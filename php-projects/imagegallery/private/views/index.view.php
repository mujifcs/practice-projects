<?php
	require getPathFor('views/partials/header.php');
?>

<div class="row">
    
    <?php
    
        while ($img = $images->fetch()) :?>
            <div class=" col-md-6 col-lg-4 mb-4">
                <a href="image?id=<?php echo urlencode($img['id'])?>">
                    <div class="card text-center">
                        <img class="card-img-top img-fluid"  style="height: 14em; max-height: 14em;" src="uploads/thumbnails/<?php echo $img['imgname'];?>" alt="image">
                        <div class="card-body">
                            <div class="rating card-text" data-rateyo-rating="<?php echo $img['avg'] ?? 0; $img =null ?>"></div>
                        </div>
                    </div>    
                </a>
            </div>
        <?php  endwhile;
        $images = null;
    ?>
    
    
</div>


<?php
	require getPathFor('views/partials/footer.php');
?>