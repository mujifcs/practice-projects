<?php


require '../private/core/bootstrap.php';

$path = getPath();

$title = getTitle($path);

require getController($path);